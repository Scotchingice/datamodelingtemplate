Message = "Update"
address = "ipy's/*.ipynb"
submit:
	git add .
	git commit -m "$(Message)"
	git push

transform:
	jupyter nbconvert --to script $(address)
