#!/usr/bin/env python
# coding: utf-8

# In[2]:


import numpy as np
import pandas as pd
from sklearn import preprocessing
data = pd.DataFrame(np.arange(12).reshape(3,4),columns = list('abcd'))
data.head()


# In[3]:


# 12 types of dimensional process
# 1.标准化 Standardization 使得均值为0，方差为1（标准差也是1）
#很多研究算法中均有使用此种处理，比如聚类分析前一般需要进行标准化处理，也或者因子分析时默认会对数据标准化处理
zscaler = preprocessing.StandardScaler()
data_standard = zscaler.fit_transform(data)
data_standard


# In[4]:


#2.区间缩放法 Min-Max Scaling 使得数据缩放到一个区间(区间为[0,1]时特化为MMS归一化)
min_max_scaler = preprocessing.MinMaxScaler()
data_minmax = min_max_scaler.fit_transform(data)
data_minmax


# 常见的归一化方法有L1正则化和L2正则化
# L1正则化是指将数据集中的每个样本的每个特征的绝对值之和为1
# L2正则化是指将数据集中的每个样本的每个特征的平方和为1
# 应用场景： 多尺度数特征缩放、稀疏数据

# In[5]:


#3.正则化 Normalization 使得每个样本的特征向量的欧式长度为1
normalizer = preprocessing.Normalizer()
data_normalized = normalizer.fit_transform(data)
data_normalized


# 正向化的目的是对正向指标保持正向且量纲化，逆向化的目的是对逆向指标正向且量纲化；比如这样一些指标GDP增长率、科研产出数量、失业率共3个指标；明显的，GDP增长率、科研产出数量是数字越大越好，而失业率是数字越小越好。
# 相当于将逆向指标逆向化后，新的数据为数字越大越好，这样便于进行方向的统一，尤其是在指标同时出现正向指标和逆向指标时，针对逆向指标进行逆向处理，是非常常见的处理方式。

# In[6]:


#4.正向化逆向化 
#假设column 1,2,3是正向指标，column 4是逆向指标
# （X-Min）/(Max-Min) 正向化
# （Max-X）/(Max-Min) 逆向化
data['a'] = (data['a']-data['a'].min())/(data['a'].max()-data['a'].min())
data['b'] = (data['b']-data['b'].min())/(data['b'].max()-data['b'].min())
data['c'] = (data['c']-data['c'].min())/(data['c'].max()-data['c'].min())
data['d'] = (data['d'].max()-data['d'])/(data['d'].max()-data['d'].min())
data


# In[6]:




